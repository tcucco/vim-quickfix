" Delete lines from quickfix with dd
autocmd FileType qf nmap <buffer> dd :call setqflist(filter(getqflist(), {idx -> idx != line('.') - 1}), 'r')<CR>

" Add current line to quickfix list
map <Leader>qa :caddexpr expand("%") . ":" . line(".") . ":" . getline(".")<CR>

" Clear the quickfix list and save
map <Leader>qc :cexpr []<CR><Leader>qs

" Save the quickfix list
map <Leader>qs :copen<CR>:w! /tmp/quickfix<CR>

" Load the saved quickfix list
map <Leader>ql :cfile /tmp/quickfix<CR>

" Add current line to quickfix list and save list
map <Leader>qas <Leader>qa<Leader>qs

" Remove the current line from the quickfix and save list
map <Leader>qr :call setqflist(filter(getqflist(), {idx -> idx != line('.') - 1}), 'r')<CR><Leader>qs

" Open prompt to positively filter quickfix list
map <Leader>qf :Cfilter<Space>

" Open prompt to negatively filter quickfix list
map <Leader>qF :Cfilter!<Space>
