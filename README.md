# VIM QuickFix

Enables `dd` to delete lines from the quickfix list, and adds a handful of
leader mappings to make working with the quickfix list more pleasant.

Includes mappings for:

- adding the current line to the quickfix list
- saving the quickfix list to a temporary file
- loading the saved quickfix list from the temporary file
- short cuts to starting quickfix list filtering
